data "template_file" "user_data" {
  for_each = var.kube-instances
  template = file("${path.module}/files/user_data.cfg")
  vars = {
    user     = var.user
    pubkey   = file("${path.module}/files/kube_ed25519.pub")
    hostname = "${each.value.name}${index(keys(var.kube-instances), each.key) > 0 ? "-${index(keys(var.kube-instances), each.key)}" : ""}"
    domain   = "${var.domain_name}"
  }
}

resource "local_file" "cloud_init_user_data_file" {
  for_each = var.kube-instances
  content  = data.template_file.user_data[each.key].rendered
  filename = "${path.module}/files/user_data_${var.base_vmid + index(keys(var.kube-instances), each.key)}.cfg"
}

resource "null_resource" "cloud_init_config_files" {
  for_each = var.kube-instances
  provisioner "local-exec" {
    command = <<-EOT
      scp -o StrictHostKeyChecking=no ${local_file.cloud_init_user_data_file[each.key].filename} ${var.pve_user}@${var.pve_host}:/tmp/user_data_vm-${var.base_vmid + index(keys(var.kube-instances), each.key)}.yml && ssh ${var.pve_user}@${var.pve_host} "sudo mv /tmp/user_data_vm-${var.base_vmid + index(keys(var.kube-instances), each.key)}.yml /var/lib/vz/snippets/"
    EOT
  }
}

# Create multiple instances of one image.
resource "proxmox_vm_qemu" "instance" {
  for_each = var.kube-instances
  depends_on = [
    null_resource.cloud_init_config_files,
  ]

  name        = "${each.value.name}${index(keys(var.kube-instances), each.key) > 0 ? "-${index(keys(var.kube-instances), each.key)}" : ""}"
  vmid        = var.base_vmid + index(keys(var.kube-instances), each.key)
  desc        = each.value.desc
  target_node = each.value.node

  clone      = var.template
  full_clone = false

  sockets = var.sockets
  cores   = var.cores
  vcpus   = each.value.vcpu
  memory  = each.value.memory
  numa    = true

  # Options
  agent   = 1
  onboot  = true
  hotplug = "network,disk,usb,cpu,memory"

  bios   = "ovmf"
  boot   = "c" # Boot order CD, Disk, Network
  scsihw = "virtio-scsi-single"

  disk {
    type     = "scsi"
    storage  = var.storage
    size     = each.value.disk_size
    format   = "raw"
    iothread = 1
    discard  = "on"
  }

  network { # cluster-network
    model   = "virtio"
    bridge  = "vmbr0"
    tag     = 6
    macaddr = lookup(var.mac-addresses, each.key, null)["if_0"]
  }

  network { # NFS-network
    model   = "virtio"
    bridge  = "vmbr3"
    macaddr = lookup(var.mac-addresses, each.key, null)["if_1"]
  }

  serial {
    id   = 0
    type = "socket"
  }

  # CloudInit
  os_type                 = "cloud-init"
  cicustom                = "user=local:snippets/user_data_vm-${var.base_vmid + index(keys(var.kube-instances), each.key)}.yml"
  cloudinit_cdrom_storage = var.storage

  ipconfig0 = "ip=10.0.8.${80 + index(keys(var.kube-instances), each.key)}/24,gw=10.0.8.1"
  ipconfig1 = "ip=8.8.1.${80 + index(keys(var.kube-instances), each.key)}/24"
}

locals {
  master_instances = { for k, v in var.kube-instances : k => v if startswith(k, "master") }
  worker_instances = { for k, v in var.kube-instances : k => v if startswith(k, "worker") }
}

resource "local_file" "ansible_inventory" {
  file_permission = "0600"
  filename        = "${path.module}/files/ansible_inventory"
  content         = <<-EOT
  [masters]
  ${join("\n", [for name, instance in local.master_instances : "${name} ansible_host=${instance.name}.${var.domain_name}"])}

  [workers]
  ${join("\n", [for name, instance in local.worker_instances : "${name} ansible_host=${instance.name}${length(keys(var.kube-instances)) > 0 ? "-" : ""}${index(keys(var.kube-instances), name)}.${var.domain_name}"])}

  [all:vars]
  ansible_python_interpreter=/usr/bin/python3
  ansible_ssh_extra_args='-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null'
  ansible_ssh_private_key_file=terraform_provision/files/kube_ed25519
  ansible_user=${var.user}
EOT
}

resource "null_resource" "cleanup" {
  provisioner "local-exec" {
    command = <<-EOT
      ssh -o StrictHostKeyChecking=no ${var.pve_user}@${var.pve_host} "echo /dev/null > .ssh/known_hosts"
    EOT
  }
}
