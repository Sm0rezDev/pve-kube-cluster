# Template to clone
template = "debian-12-template"

user = "kube" # Cloud user

storage = "vm-pool" # Storage pool

domain_name = "sm0rez.internal"

base_vmid = 800 # Starting VM-ID

sockets = 2 # Number of sockets

cores = 16 # Maxinum number of cores per socket

kube-instances = { # Instance map
  master_0 = {
    "node"      = "exodus",
    "name"      = "galatic-commander",
    "desc"      = "kube master",
    "memory"    = 4096,
    "vcpu"      = 4,
    "disk_size" = "16G"
  }

  worker_0 = {
    "node"      = "exodus",
    "name"      = "stellar-crew",
    "desc"      = "kube worker node",
    "memory"    = 16384,
    "vcpu"      = 16,
    "disk_size" = "16G"
  }

  worker_1 = {
    "node"      = "exodus",
    "name"      = "cosmic-pioneer",
    "desc"      = "kube worker node",
    "memory"    = 16384,
    "vcpu"      = 16,
    "disk_size" = "16G"
  }

  worker_2 = {
    "node"      = "exodus",
    "name"      = "interstellar-navigator",
    "desc"      = "kube worker node",
    "memory"    = 16384,
    "vcpu"      = 16,
    "disk_size" = "16G"
  }

  worker_3 = {
    "node"      = "exodus",
    "name"      = "starlight-rift",
    "desc"      = "kube worker node",
    "memory"    = 16384,
    "vcpu"      = 16,
    "disk_size" = "16G"
  }

  worker_4 = {
    "node"      = "exodus",
    "name"      = "celestial-solaris",
    "desc"      = "kube worker node",
    "memory"    = 16384,
    "vcpu"      = 16,
    "disk_size" = "16G"
  }

  worker_5 = {
    "node"      = "exodus",
    "name"      = "cosmos-vertex",
    "desc"      = "kube worker node",
    "memory"    = 16384,
    "vcpu"      = 16,
    "disk_size" = "16G"
  }

  worker_6 = {
    "node"      = "exodus",
    "name"      = "nebula-quasar",
    "desc"      = "kube worker node",
    "memory"    = 16384,
    "vcpu"      = 16,
    "disk_size" = "16G"
  }

  worker_7 = {
    "node"      = "exodus",
    "name"      = "galactic-zenon",
    "desc"      = "kube worker node",
    "memory"    = 16384,
    "vcpu"      = 16,
    "disk_size" = "16G"
  }
}

mac-addresses = { # Map of MAC-Addresses
  master_0 = {
    "if_0" = "e2:c3:d9:c7:57:5e",
    "if_1" = "e2:72:d5:76:ab:67"
  }
  worker_0 = {
    "if_0" = "62:db:49:28:26:ae",
    "if_1" = "72:fa:10:db:99:f5"
  }
  worker_1 = {
    "if_0" = "ae:75:34:51:f0:20",
    "if_1" = "de:d7:ec:2f:f0:4e"
  }
  worker_2 = {
    "if_0" = "0a:2b:b7:13:50:25",
    "if_1" = "0a:54:9e:65:48:49"
  }
  worker_3 = {
    "if_0" = "3A:0C:BC:5B:35:68",
    "if_1" = "5A:6E:2D:3D:60:42"
  }
  worker_4 = {
    "if_0" = "6A:63:D1:7E:DD:9F",
    "if_1" = "66:56:CB:D7:7D:CB"
  }
  worker_5 = {
    "if_0" = "D2:20:2D:85:E9:7C",
    "if_1" = "EE:BE:15:44:C7:7A"
  }
  worker_6 = {
    "if_0" = "AA:D6:29:E6:17:54",
    "if_1" = "BA:5C:8E:D3:61:CC"
  }
  worker_7 = {
    "if_0" = "2A:32:EF:9F:F1:C6",
    "if_1" = "BE:AD:C7:8F:E0:AF"
  }
}
