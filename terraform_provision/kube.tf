# Variable definition.
variable "template" {
  type = string
}

variable "pool" {
  type    = string
  default = ""
}

# CloudInit
variable "user" {
  type    = string
  default = "root"
}

variable "storage" {
  type    = string
  default = "local-zfs"
}

variable "domain_name" {
  type    = string
  default = "local"
}

variable "pve_host" {
  type    = string
  default = "pve.localhost"
}

variable "pve_user" {
  type    = string
  default = "root"
}

variable "base_vmid" {
  type    = number
  default = 100
}

variable "sockets" {
  type    = number
  default = 1
}

variable "cores" {
  type    = number
  default = 1
}

# Node parameters.
variable "kube-instances" {
  description = "vm-config"
  type        = map(any)
  default = {
    "node"      = "pve",
    "name"      = "kube",
    "desc"      = "node"
    "memory"    = 1024,
    "sockets"   = 1,
    "vcpu"      = 1,
    "disk_size" = "4G",
  }
}

variable "mac-addresses" {
  description = "mac addresses"
  type        = map(any)
}
