terraform {
  required_providers {
    proxmox = {
      source = "Telmate/proxmox"
      # version = "2.9.11"
      version = "2.9.14"
    }
  }
  backend "http" {}
}

provider "proxmox" {
  pm_parallel = 32
  pm_debug    = true
}
