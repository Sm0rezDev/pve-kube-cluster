# Proxmox Kubernetes Cluster

[[_TOC_]]

## Environment Variable

* PM_API_URL
    - Proxmox API URL.

* PM_API_TOKEN_ID
    - Proxmox API TOKEN ID.

* PM_API_TOKEN_SECRET
    - Proxmox API TOKEN SECRET

* TF_VAR_pve_user
    - Proxmox user

* TF_VAR_pve_host
    - Proxmox host

* HOST_RSA
    - Proxmox Host private key.

* KUBE_RSA
    - Private key.

* KUBE_RSA_PUB
    - Public key.

